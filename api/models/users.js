var db = require('../config/mongo_database');
var jwt = require('jsonwebtoken');
var secret = require('../config/secret');
var tokenManager = require('../config/token_manager');
var User = new db.userModel();

// Add user
exports.register = function(req, res) {
	var username = req.body.username || '';
	var password = req.body.password || '';
	var passwordConfirmation = req.body.passwordConfirmation || '';
	if (username == '' || password == '' || password != passwordConfirmation) {
		return res.send(400);
	}
	var user = new db.userModel();
	user.username = username;
	user.password = password;
	user.save(function(err) {
		if (err) {
			console.log(err);
			return res.send(500);
		}
		db.userModel.count(function(err, counter) {
			if (err) {
				console.log(err);
				return res.send(500);
			}
			if (counter == 1) {
				db.userModel.update({username:user.username}, {is_admin:true}, function(err, nbRow) {
					if (err) {
						console.log(err);
						return res.send(500);
					}
					console.log('First user created as an Admin');
					return res.send(200);
				});
			}
			else {
				return res.send(200);
			}
		});
	});
};

// Login user
exports.login = function(req, res) {
	var username = req.body.username || '';
	var password = req.body.password || '';
	if (username == '' || password == '') {
		return res.send(401);
	}
	db.userModel.findOne({username: username}, function (err, user) {
		if (err) {
			console.log(err);
			return res.send(401);
		}
		if (user == undefined) {
			return res.send(401);
		}
		user.comparePassword(password, function(isMatch) {
			if (!isMatch) {
				console.log("Attempt failed to login with " + user.username);
				return res.send(401);
			}
			var token = jwt.sign({id: user._id}, secret.secretToken, { expiresInMinutes: tokenManager.TOKEN_EXPIRATION });
			return res.json({token:token});
		});
	});
};

// Logout user
exports.logout = function(req, res) {
	if (req.user) {
		tokenManager.expireToken(req.headers);
		delete req.user;
		return res.send(200);
	}
	else {
		return res.send(401);
	}
};

// Delete user
exports.delete = function(req, res) {
	User.remove({
		_id : req.params.user_id
	}, function(err, user) {
		if (err)
			return res.send(err);
		return res.json(user);
	});
};

// Get user
exports.getUser = function(req, res) {
	User.findById(req.params.user_id, function(err, user) {
		if (err)
			return res.send(err);
		return res.json(user);
	});
};

// Update user
exports.update = function(req, res) {
	User.findById(req.params.user_id, function(err, bill) {
		if (err)
			return res.send(err);
		User.name = req.body.name; 	// update the user info

		User.save(function(err, user) {
			if (err)
				return res.send(err);
			return res.json(user);
		});
	});
};