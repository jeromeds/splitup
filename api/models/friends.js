var db = require('../config/mongo_database');
var jwt = require('jsonwebtoken');
var secret = require('../config/secret');
var tokenManager = require('../config/token_manager');
var Friend = new db.friendModel();

// Get all friends
exports.getFriends = function(req, res) {
	Friend.find(function(err, friends) {
		if (err)
			return res.send(err)
		return res.json(friends);
	});
};

// Add friend
exports.post = function(req, res) {
	Friend.create({
		displayname : req.body.displayname
	}, function(err, friend) {
		if (err)
			return res.send(err);
		// todo: make a function for 'find'. it's repeated everywhere.
		Friend.find(function(err, friends) {
			if (err)
				return res.send(err)
			return res.json(friends);
		});
	});
}

// Delete friend
exports.delete = function(req, res) {
	Friend.remove({
		_id : req.params.friend_id
	}, function(err, people) {
		if (err)
			return res.send(err);
		// todo: make a function for 'find'. it's repeated everywhere.
		Friend.find(function(err, friends) {
			if (err)
				return res.send(err)
			return res.json(friends);
		});
	});
};

// Get friend profile
exports.getFriend = function(req, res) {
	Friend.findById(req.params.friend_id, function(err, friend) {
		if (err)
			return res.send(err);
		return res.json(friend);
	});
};

// Update a friend profile
exports.update = function(req, res) {
	Friend.findById(req.params.friend_id, function(err, friend) {
		if (err)
			return res.send(err);
		Friend.displayname = req.body.displayname; 	// update the friend info
		// save the friend
		Friend.save(function(err, friend) {
			if (err)
				return res.send(err);
			return res.json(friend);
		});
	});
};