var db = require('../config/mongo_database');
var jwt = require('jsonwebtoken');
var secret = require('../config/secret');
var tokenManager = require('../config/token_manager');
var Bill = new db.billModel();

// Get bill
exports.getBills = function(req, res) {
	Bill.find(function(err, bills) {
		if (err)
			return res.send(err);
		return res.json(bills);
	});
};

// Add bill
exports.post = function(req, res) {
	Bill.create({
		id_user: req.body.id_user,
		id_people: req.body.id_people,
		amount: req.body.amount
	}, function(err, bill) {
		if (err)
			return res.send(err);
		// todo: make a function for 'find'. it's repeated everywhere.
		Bill.find(function(err, bills) {
			if (err)
				return res.send(err)
			return res.json(bills);
		});
	});
};

// Delete bill
exports.delete = function(req, res) {
	Bill.remove({
		_id : req.params.bill_id
	}, function(err, bill) {
		if (err)
			return res.send(err);
		Bill.find(function(err, bills) {
			if (err)
				return res.send(err)
			return res.json(bills);
		});
	});
};

// Get bill
exports.getBill = function(req, res) {
	Bill.findById(req.params.bill_id, function(err, bill) {
		if (err)
			return res.send(err);
		return res.json(bill);
	});
}

// Update bill
exports.update = function(req, res) {
	Bill.findById(req.params.bill_id, function(err, bill) {
		if (err)
			return res.send(err);
		Bill.id_people = req.body.id_people;
		Bill.save(function(err, bill) {
			if (err)
				return res.send(err);
			return res.json(bill);
		});
	});
};

