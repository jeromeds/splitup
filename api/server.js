// source: http://scotch.io/tutorials/javascript/creating-a-single-page-todo-app-with-node-and-angular

// set up ========================
var express  = require('express');
var app      = express(); 								// create our app w/ express
var jwt = require('express-jwt');
var mongoose = require('mongoose'); 					// mongoose for mongodb
var morgan = require('morgan'); 			// log requests to the console (express4)
var bodyParser = require('body-parser'); 	// pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)

var tokenManager = require('./config/token_manager');
var secret = require('./config/secret');

// configuration =================
//mongoose.connect('mongodb://localhost'); 	// connect to mongoDB database on modulus.io
//HELP: if connection not working: http://www.marcusoft.net/2014/05/mongodb-and-10309-unable-to-createopen.html
app.use(express.static(__dirname + '/public')); 				// set the static files location /public/img will be /img for users
app.use(morgan('dev')); 										// log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'})); 			// parse application/x-www-form-urlencoded
app.use(bodyParser.json()); 									// parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());

// routes ======================================================================

//Routes
var routes = {};
//routes.posts = require('./route/posts.js');
routes.users = require('./models/users.js');
routes.friends = require('./models/friends.js');
routes.bills = require('./models/bills.js');

// api ---------------------------------------------------------------------
         // EXAMPLE AUTH https://github.com/kdelemme/blogjs
// ex2: http://scotch.io/tutorials/javascript/easy-node-authentication-setup-and-local
// == this is the one i thinkg: https://github.com/linnovate/mean
app.all('/*', function(req, res, next) {
	res.set('Access-Control-Allow-Origin', 'http://127.0.0.1:9050');
	res.set('Access-Control-Allow-Credentials', true);
	res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
	res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
	if ('OPTIONS' == req.method) return res.send(200);
	next();
});

/**********
 * Users
 */
// Create a new user
app.post('/user/register', routes.users.register);

// Login
app.post('/user/login', routes.users.login);

// Logout
app.get('/user/logout', jwt({secret: secret.secretToken}), routes.users.logout);

// Get user
app.get('/api/users/:user_id', routes.users.getUser);

// Update user
app.put('/api/users/:user_id', routes.users.update);

// Delete user
app.delete('/api/users/:user_id',routes.users.delete);

/**********
 * Friends
 */
// Get all friends
app.get('/api/friends', routes.friends.getFriends);

// Add friend
app.post('/api/friend', routes.friends.post);

// Delete friend
app.delete('/api/friend/:friend_id', routes.friends.delete);

// Get friend
app.get('/api/friend/:friend_id', routes.friends.getFriend);

// Update friend
app.put('/api/friend/:friend_id', routes.friends.update);

/**********
 * Bills
 */
// Get all bills
app.get('/api/bills', routes.bills.getBills);

// Add bill
app.post('/api/friend', routes.bills.post);

// Delete bill
app.delete('/api/bill/:bill_id', routes.bills.delete);

// Get bill
app.get('/api/bill/:bill_id', routes.bills.getBill);

// Update bill
app.put('/api/friend/:bill_id', routes.bills.update);

// listen (start app with node server.js) ======================================
app.listen(8080);
console.log("App listening on port 8080");
