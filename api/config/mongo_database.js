var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var SALT_WORK_FACTOR = 10;
var mongodbURL = 'mongodb://localhost';
var mongodbOptions = { };

mongoose.connect(mongodbURL, mongodbOptions, function (err, res) {
	if (err) {
		console.log('Connection refused to ' + mongodbURL);
		console.log(err);
	} else {
		console.log('Connection successful to: ' + mongodbURL);
	}
});

var Schema = mongoose.Schema;

// User schema
var User = new Schema({
	username: { type: String, required: true, unique: true },
	password: { type: String, required: true },
	is_admin: { type: Boolean, default: false },
	created: { type: Date, default: Date.now }
});

// Friend schema
var Friend  = new Schema({
	displayname: { type: String, required: true, unique: true },
	password: { type: String, required: true },
	created: { type: Date, default: Date.now }
});

// Bill schema
var Bill = new Schema({
	id_user : { type: String, required: true, unique: true },
	id_friend : { type: String, required: true },
	amount : { type: Number, default: 0, required: true  },
	created: { type: Date, default: Date.now },
	is_paid: { type: Boolean, default: false }
});

// Bcrypt middleware on UserSchema
User.pre('save', function(next) {
	var user = this;

	if (!user.isModified('password')) return next();

	bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
		if (err) return next(err);

		bcrypt.hash(user.password, salt, function(err, hash) {
			if (err) return next(err);
			user.password = hash;
			next();
		});
	});
});

// Password verification
User.methods.comparePassword = function(password, cb) {
	bcrypt.compare(password, this.password, function(err, isMatch) {
		if (err) return cb(err);
		cb(isMatch);
	});
};

//Define Models
var userModel = mongoose.model('User', User);
var friendModel = mongoose.model('Friend', Friend);
var billModel = mongoose.model('Bill', Bill);

// Export Models
exports.userModel = userModel;
exports.friendModel = friendModel;
exports.billModel = billModel;
