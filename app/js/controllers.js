'use strict';

var appControllers = angular.module('appControllers', []);

appControllers.controller('MenuCtrl', ['$scope', '$state', '$window', 'UserService', 'AuthenticationService',
	function ($scope, $state, $window, UserService, AuthenticationService) {

		$scope.logOut = function logOut() {
			if (AuthenticationService.isAuthenticated) {
				 UserService.logOut().success(function(data) {
					 AuthenticationService.isAuthenticated = false;
					 delete $window.sessionStorage.token;
					 $state.go("register");
				 }).error(function(status, data) {
					 console.log(status);
					 console.log(data);
				 });
			} else {
				$state.go("login");
			}
		};

	}
]);

appControllers.controller('RegisterCtrl', ['$scope', '$state', '$window', 'UserService', 'AuthenticationService',
	function ($scope, $state, $window, UserService, AuthenticationService) {
		//source http://www.kdelemme.com/2014/03/09/authentication-with-angularjs-and-a-node-js-rest-api/
		//Admin User Controller (signIn, logOut)
		$scope.logIn = function logIn(username, password) {
			if (username != null && password != null) {

				UserService.logIn(username, password).success(function(data) {
					AuthenticationService.isAuthenticated = true;
					$window.sessionStorage.token = data.token;
					$state.go("dashboard");
				}).error(function(status, data) {
						console.log(status);
						console.log(data);
					});
			}
		};
		$scope.logOut = function logOut() {
			console.log('logout');
			/*if (AuthenticationService.isAuthenticated) {
				UserService.logOut().success(function(data) {
					AuthenticationService.isAuthenticated = false;
					delete $window.sessionStorage.token;
					$state.go("register");
				}).error(function(status, data) {
						console.log(status);
						console.log(data);
					});
			}
			else {
				$location.path("/admin/login");
			}*/
		};
		$scope.register = function register(username, password, passwordConfirm) {
			if (AuthenticationService.isAuthenticated) {
				$state.go("dashboard");
			}
			else {
				UserService.register(username, password, passwordConfirm).success(function(data) {
					$state.go("login");
				}).error(function(status, data) {
						console.log(status);
						console.log(data);
					});
			}
		};
	}
]);
appControllers.controller('LoginCtrl', function ($scope, $state) {
	$scope.submit = function(){
		//$state.go('add-user') ;
	};
});
appControllers.controller('ForgotPasswordCtrl', function ($scope, $state) {
	$scope.submit = function(){
		console.log('forgot password submit');
	};
});
appControllers.controller('PeopleAddCtrl', ['$scope', '$state', 'peoplesService',
	function ($scope, $state, peoplesService) {
		var getPeoples = function(){
			peoplesService.all()
				.success(function(res){
					$scope.peoples = res;
				})
		};

		// Add new people
		$scope.submit = function(){
			peoplesService.add($scope.form)
				.success(function(res){
					$scope.form = {};
					$scope.peoples = res;
				});
		};

		// Delete people
		$scope.delete = function(id){
			peoplesService.delete(id)
				.success(function(res){
					$scope.peoples = res;
				});
		};

		// Init
		getPeoples();
	}
]);
appControllers.controller('DashboardCtrl', ['$scope', '$state', 'peoplesService', 'billsService',
	function ($scope, $state, peoplesService, billsService) {
		$scope.amounts = [];
		$scope.submit = function(){
			console.log('submit');
			addBills();
		};

		// Get all people
		var getPeoples = function(){
			peoplesService.all()
				.success(function(res){
					$scope.peoples = res;
				})
		};

		// Get all bills
		var getBills = function(){
			billsService.all()
				.success(function(res){
					$scope.bills = res;
					console.log(res);
				})
		};

		// Add bills
		var addBills = function(){
			billsService.add({amount : $scope.amount})
				.success(function(res){
					$scope.bills = res;
					console.log(res);
				})
		};

		// Update bills
		$scope.updateBill = function(amount_id, people_id){
			billsService.update(amount_id, {id_people : people_id})
				.success(function(res){
					$scope.bills = res;
				});
		};

		// Init
		getPeoples();
		getBills();
	}
]);



