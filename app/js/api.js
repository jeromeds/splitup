'use strict';
    // Auth: https://medium.com/opinionated-angularjs/techniques-for-authentication-in-angularjs-applications-7bbf0346acec
var appAPI = angular.module('appAPI', ['constants']);

appAPI.factory('UserService', function(API_ROOT, $http) {
	return {
		logIn: function(username, password) {
			return $http.post(API_ROOT + '/user/login', {username: username, password: password});
		},
		logOut: function() {
			return $http.get(API_ROOT + '/user/logout');
		},
		register: function(username, password, passwordConfirmation) {
			return $http.post(API_ROOT +  '/user/register', {username: username, password: password, passwordConfirmation: passwordConfirmation});
		}
	}
});
	/*.factory('UserService', function(API_ROOT, $http) {
		return {
			all : function() {
				return $http.get(API_ROOT + '/api/users');
			},
			get : function(id) {
				return $http.get(API_ROOT + '/api/users/' + id);
			},
			create : function(data) {
				return $http.post(API_ROOT + '/api/users', data);
			},
			update : function(id, data) {
				return $http.put(API_ROOT + '/api/users/' + id, data);
			},
			delete : function(id) {
				return $http.delete(API_ROOT + '/api/users/' + id);
			},
			logIn: function(username, password) {
				return $http.post(API_ROOT + '/api/login', {username: username, password: password});
			},
			logOut: function() {

			}
		}
	});*/
appAPI.factory('peoplesService', function(API_ROOT, $http) {
	return {
		all : function() {
			return $http.get(API_ROOT + '/api/peoples');
		},
		get : function(id) {
			return $http.get(API_ROOT + '/api/peoples/' + id);
		},
		add : function(data) {
			return $http.post(API_ROOT + '/api/peoples', data);
		},
		update : function(id, data) {
			return $http.put(API_ROOT + '/api/peoples/' + id, data);
		},
		delete : function(id) {
			return $http.delete(API_ROOT + '/api/peoples/' + id);
		}
	}
});
appAPI.factory('billsService', function(API_ROOT,$http) {
	return {
		all : function() {
			return $http.get(API_ROOT + '/api/bills');
		},
		get : function(id) {
			return $http.get(API_ROOT + '/api/bills/' + id);
		},
		add : function(data) {
			return $http.post(API_ROOT + '/api/bills', data);
		},
		update : function(id, data) {
			return $http.put(API_ROOT + '/api/bills/' + id, data);
		},
		delete : function(id) {
			return $http.delete(API_ROOT + '/api/bills/' + id);
		}
	}
});

