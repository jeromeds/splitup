'use strict';

var app = angular.module('splitupApp', [
	//'ngCookies',
	//'ngResource',
	//'ngSanitize',
	'ui.router',
	'ui.bootstrap',
	'appAPI',
	'appControllers',
	'appServices'
]);
app.config(['$stateProvider', '$urlRouterProvider',
	function ($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/register');
		$stateProvider
			.state('register', {
				url: '/register',
				templateUrl: 'views/register.html',
				controller: 'RegisterCtrl' ,
				access: { requiredLogin: false }
			})
			.state('login', {
				url: '/login',
				templateUrl: 'views/login.html',
				controller: 'RegisterCtrl',
				access: { requiredLogin: false }
			})
			.state('logout', {
				url: '/logout',
				templateUrl: 'views/logout.html',
				controller: 'AdminUserCtrl',
				access: { requiredLogin: true }
			})
			.state('forgot-password', {
				url: '/register',
				templateUrl: 'views/forgot-password.html',
				controller: 'ForgotPasswordCtrl',
				access: { requiredLogin: false }
			})
			.state('add-people', {
				url: '/people/add',
				templateUrl: 'views/people-add.html',
				controller: 'PeopleAddCtrl',
				access: { requiredLogin: true }
			})
			.state('dashboard', {
				url: '/dashboard',
				templateUrl: 'views/dashboard.html',
				controller: 'DashboardCtrl'  ,
				access: { requiredLogin: true }
			});
	}
]);
//todo: http://bitoftech.net/2014/06/09/angularjs-token-authentication-using-asp-net-web-api-2-owin-asp-net-identity/
app.config(function ($httpProvider) {
	var requestInterceptor = ['$q', '$rootScope', '$window', 'AuthenticationService',
		function ($q, $rootScope, $window, AuthenticationService) {
			var interceptorInstance = {
				request: function (config) {
					config.headers = config.headers || {};
					if ($window.sessionStorage.token) {
						config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
					}
					return config;
				},
				requestError: function(rejection) {
					return $q.reject(rejection);
				},
				/* Set Authentication.isAuthenticated to true if 200 received */
				response: function (response) {
					if (response != null && response.status == 200 && $window.sessionStorage.token && !AuthenticationService.isAuthenticated) {
						AuthenticationService.isAuthenticated = true;
					}
					return response || $q.when(response);
				},
				/* Revoke client authentication if 401 is received */
				responseError: function(rejection) {
					if (rejection != null && rejection.status === 401 && ($window.sessionStorage.token || AuthenticationService.isAuthenticated)) {
						delete $window.sessionStorage.token;
						AuthenticationService.isAuthenticated = false;
						$state.go("login");
					}
					return $q.reject(rejection);
				}
			};
			return interceptorInstance;
		}];

	$httpProvider.interceptors.push(requestInterceptor);

});
app.run(function($rootScope, $state, $window, AuthenticationService) {
	$rootScope.$on("$stateChangeStart", function(event, nextRoute, currentRoute) {
		//redirect only if both isAuthenticated is false and no token is set
		if (nextRoute != null && nextRoute.access != null && nextRoute.access.requiredAuthentication
			&& !AuthenticationService.isAuthenticated && !$window.sessionStorage.token) {
			$state.go("login");
		}
	});

	$rootScope.$on('$stateChangeError',
		function(event, toState, toParams, fromState, fromParams, error){
			//console.log('stateChangeError');
			//console.log(toState, toParams, fromState, fromParams, error);
			if (toState != null && toState.access != null && toState.access.requiredAuthentication
				&& !AuthenticationService.isAuthenticated && !$window.sessionStorage.token) {
				$state.go("login");
			}
			/*if(error.status == 401){
				console.log("401 detected. Redirecting...");
				//authService.deniedState = toState.name;
				$state.go("login");
			}*/
		});
});



