# Split Up #

#Dev notes
##API SERVER
To start the API in dev:
- Launch 'mongod' command
- go to the api folder and launch 'node server.js'

##App
At the root launch 'grunt serve' command

#Roadmap
- Refactor node server.
- Finish auth module in angularJS
- Add LocalStorage in order to store the token session
- Add touch event
- Drag & drop on circle to replace some button
- Make it work for more then 2 player per user